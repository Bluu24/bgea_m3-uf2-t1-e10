# **bgea_M3-UF2–T1–E10**
 
 ## **UF1. Disseny descendent**
 <br>

## INDEX
- ### [Exercici 12:Donat un text calcular la seva longitud ](#ex-12)

- ### [Exercici 13:Donat un text comptar el nombre de vocals.](#ex-13)

- ### [Exercici 14:Donat un text comptar el nombre de consonants](#ex-14)

- ### [Exercici 15:Donat un text Capgirar-lo.](#ex-15)

- ### [Exercici 16:Donat un text Comptar el nombre de paraules que acaben en “ts”.](#ex-16)

- ### [Exercici 17:Donat un text comptar el nombre de paraules](#ex-17)

- ### [Exercici 18:Donat un text dissenyeu un algorisme que compti els cops de apareixen conjuntament la parella de caràcters “as” dins del text](#ex-18)

- ### [Exercici 19:Donat un text (text) i una paraula (parbus). Dissenyeu un algorisme que comprovi si la paraula és troba dins del text.](#ex-19)

- ### [Exercici 20:Donat un text i una paraula (parbus). Dissenyeu un algorisme que digui quants cops apareix la paraula (parbus) dins del text ](#ex-20)

- ### [Exercici 21:Donat un text, una paraula (parbus), i una paraula (parsub), dissenyeu un algorisme que substitueixi, en el text, totes les vegades que apareix la paraula (parbus) per la paraula (parsub).](#ex-21)
<br>

## EX 12:
---
**Donat un text calcular la seva longitud**

```c
int longitud(char text[]);

int main()
{
    char text[]="hola mon";
    printf("La longitud: %i",longitud(text));
}
    //EX 12
int longitud(char text[]){
    int i=0;
    while(text[i]!='\0') i++;
    return i;
```


![Calcular la seva longitud](/imatges/12.png)
<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 13:
---
**Donat un text comptar el nombre de vocals**
```c
int nVovals(char text[]);

int main()
{
    char text[]="hola";
    printf("\n Num Vocals: %i",nVovals(text));
}

int nVovals(char text[]){
    int i=0;
    int comptador=0;
    while(text[i]!='\0'){
        if(text[i]=='a' ||
           text[i]=='e' ||
           text[i]=='i' ||
           text[i]=='o' ||
           text[i]=='u' ||
           text[i]=='A' ||
           text[i]=='E' ||
           text[i]=='I' ||
           text[i]=='O' ||
           text[i]=='U')comptador++;
           i++;
    }
    return comptador;
}
 
```

![Numero de Vocals](/imatges/13.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 14:
---
**Donat un text comptar el nombre de consonants**

```c
int nContsonants(char text[]);

int main()
{
    char text[]="hola";
    printf("\n Num Consonants: %i",nContsonants(text));
}
int nContsonants(char text[]){
    int i=0;
    int comptador=0;
    while(text[i]!='\0'){
        if(text[i]=='b' ||
           text[i]=='c' ||
           text[i]=='d' ||
           text[i]=='f' ||
           text[i]=='g' ||
           text[i]=='h' ||
           text[i]=='j' ||
           text[i]=='k' ||
           text[i]=='l' ||
           text[i]=='m' ||
           text[i]=='n' ||
           text[i]=='p' ||
           text[i]=='q' ||
           text[i]=='r' ||
           text[i]=='s' ||
           text[i]=='t' ||
           text[i]=='v' ||
           text[i]=='w' ||
           text[i]=='x' ||
           text[i]=='y' ||
           text[i]=='z' )comptador++;
           i++;
    }
    return comptador;
}
```

![Numero de Consonants ](/imatges/14.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 15:
---
**Donat un text capgirar-lo.**
```c
void capgirar(char text[], char textc[]);

int main()
{
    char text[]="roma";
    char textc[sizeof(text)];
    capgirar(text,textc);
    printf("\n Text Capgirat -> %s",textc);
}
void capgirar(char text[], char textc[]){
    int i=longitud(text)-1;
    int j=0;
    while(i>=0){
        textc[j]=text[i];
        i--;
        j++;
    }
    textc[j]='\0';
}

```

![text capgirar-lo](/imatges/15.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 16:
---
**Donat un text comptar el nombre de paraules que acaben en “ts”.**
```c
#include <stdbool.h>

bool acabaTS(char paraulaTS[]);
int nParTS(char text[]);
void saltarBlancs(char text[], int *i);
void obtenirParaula(char text[], int *i, char partmp[]);

int main()
{
    char text[]="holats que tats";
    printf("\n El numero de ts: %i ", nParTS(text));
}
int nParTS(char text[]){
    int i=0;
    int cTS=0;
    char parTmp[sizeof(text)];
    while (text[i]!='\0'){
        saltarBlancs(text,&i);
        obtenirParaula(text,&i,parTmp);
        if(acabaTS(parTmp)) cTS++;
    }
    return cTS;

}

bool acabaTS(char paraulaTS[]){
    bool correcte=false;
     if (longitud(paraulaTS)>1){
        if (paraulaTS[longitud(paraulaTS)-1]=='s' && paraulaTS[longitud(paraulaTS)-2]=='t') correcte=true;
     }
     return correcte;

}

void saltarBlancs(char text[], int *i){
    while(text[*i]==' ') (*i)++;
}

void obtenirParaula(char text[], int *i, char partmp[]){
    int ip=0;

    while(text[*i]!='\0' && text[*i]!=' '){
        partmp[ip]=text[*i];
        (*i)++;
        ip++;
    }
    partmp[ip]='\0';

}

```
![Numero de TS](/imatges/16.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 17:
---
**Donat un text comptar el nombre de paraules.**
```c
void saltarBlancs(char text[], int *i);
void obtenirParaula(char text[], int *i, char partmp[]);
int nPar(char text[]);

int main()
{
    char text[]="hola que tal";
    printf("\n El numero de paraules : %i ",nPar(text));
}
int nPar(char text[]){
    int i=0;
    int contador=0;
    char parTmp[sizeof(text)];
    while (text[i]!='\0'){
        saltarBlancs(text,&i);
        obtenirParaula(text,&i,parTmp);
        if(parTmp[0]!='\0')contador++;
    }
    return contador;

}

void saltarBlancs(char text[], int *i){
    while(text[*i]==' ') (*i)++;
}

void obtenirParaula(char text[], int *i, char partmp[]){
    int ip=0;

    while(text[*i]!='\0' && text[*i]!=' '){
        partmp[ip]=text[*i];
        (*i)++;
        ip++;
    }
    partmp[ip]='\0';

}
```

![Comptar el nombre de paraules](/imatges/17.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 18:
---
**Donat un text dissenyeu un algorisme que compti els cops de apareixen conjuntament la parella de caràcters “as” dins del text.** 
```c
#include <stdbool.h>

void saltarBlancs(char text[], int *i);
void obtenirParaula(char text[], int *i, char partmp[]);
int nPar(char text[]);
bool acabaAS(char paraulaAS[]);

int main()
{
    char text[]="hasla que tal";
    printf("\n El numero de paraules es: %i ",nParAS(text));
}
int nParAS(char text[]){
    int i=0;
    int cAS=0;
    char parTmp[sizeof(text)];
    while (text[i]!='\0'){
        saltarBlancs(text,&i);
        obtenirParaula(text,&i,parTmp);
        if(acabaAS(parTmp)) cAS++;
    }
    return cAS;
}

bool acabaAS(char paraulaAS[]){
    bool correcte=false;
     if (longitud(paraulaAS)>1){
        if (paraulaAS[longitud(paraulaAS)]=='a' && paraulaAS[longitud(paraulaAS)+1]=='s') correcte=true;
     }
     return correcte;

}

void saltarBlancs(char text[], int *i){
    while(text[*i]==' ') (*i)++;
}

void obtenirParaula(char text[], int *i, char partmp[]){
    int ip=0;

    while(text[*i]!='\0' && text[*i]!=' '){
        partmp[ip]=text[*i];
        (*i)++;
        ip++;
    }
    partmp[ip]='\0';

}
```
![Numero de AS](/imatges/18.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 19:
---
**Donat un text (text) i una paraula (parbus). Dissenyeu un algorisme que comprovi si la paraula és troba dins del text.**

```c
#include <stdbool.h>
#define MAX_PAR 100
#define MAX_TEXT 100
#define BB while (getchar()!='\n')

void saltarBlancs(char text[], int *i);
void entradaText(char text[]);
void entradaPar(char par[]);
void copiaParaulaTemporal(char text[],int*i,char ptmp[]);
void iguals (char par[],char ptmp[]);
void trobaParaula (char text[],char par[]);

int main()
{
    char text[MAX_TEXT+1],par[MAX_PAR+1];

    entradaText(text);
    entradaPar(par);

    trobaParaula(text,par);
}
void entradaText(char text[]){
   printf("TEXT:");
    scanf("%100[^\n]",text);BB;
}

void entradaPar(char par[]){
    printf("PARAULA:");
    scanf("%100[^\n]",par);BB;
}

void copiaParaulaTemporal(char text[],int*i,char ptmp[]){
          int itmp=0;
          while(text[*i]!=' ' && text[*i]!='\0'){
                ptmp[itmp]=text[*i];
                itmp++;
                (*i)++;

          }
         ptmp[itmp]='\0';
}

void iguals (char par[],char ptmp[]){
    int ip;
    ip=0;
    while(par[ip]!='\0' && ptmp[ip]!='\0'){
          if(par[ip] != ptmp[ip])break;
          ip++;
    }
    if(par[ip]=='\0' && ptmp[ip]=='\0'){
        printf("\nParaula Trobada");
    }
}

void saltarBlancs(char text[], int *i){
    while(text[*i]==' ') (*i)++;
}

void trobaParaula (char text[],char par[]){
    int i,it;
    char ptmp[MAX_PAR];

    while(text[i]!='\0'){
        saltarBlancs(text, &i);
        copiaParaulaTemporal(text,&i,ptmp);
        iguals(par,ptmp);
    }

}

```
<div style="color:#4086ff">TORBAT</div>

![Paraula Trobada](/imatges/19_si.png)

<div style="color:#4086ff">NO TROBAT</div>

![Paraula no Trobada](/imatges/19_no.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 20:
---
**Donat un text i una paraula (parbus). Dissenyeu un algorisme que digui quants cops apareix la paraula (parbus) dins del text.**

```c
#define MAX_PAR 100
#define MAX_TEXT 100
#define BB while (getchar()!='\n')

void entrada(char text[]);
void entradaP(char par[]);
void copiaParTemporal(char text[],int*i,char ptmp[]);
int contadorPar (char par[],char ptmp[]);
void num_ParaulesRepes(char text[],char par[]);

int main()
{
    char text[MAX_TEXT+1],par[MAX_PAR+1];
    entrada(text);
    entradaP(par);
    num_ParaulesRepes(text,par);
}
void entrada(char text[]){
   printf("TEXT:");
    scanf("%100[^\n]",text);BB;
}

void entradaP(char par[]){
    printf("PARAULA:");
    scanf("%100[^\n]",par);BB;
}

void copiaParTemporal(char text[],int*i,char ptmp[]){
          int itmp=0;
          while(text[*i]!=' ' && text[*i]!='\0'){
                ptmp[itmp]=text[*i];
                itmp++;
                (*i)++;
          }
         ptmp[itmp]='\0';
}

int contadorPar (char par[],char ptmp[]){
    int ip,contador;
    ip=0;
    contador=0;
    while(par[ip]!='\0' && ptmp[ip]!='\0'){
          if(par[ip] != ptmp[ip])break;
          ip++;
    }
    if(par[ip]=='\0' && ptmp[ip]=='\0'){
        contador++;
    }
    return contador;
}

void num_ParaulesRepes(char text[],char par[]){
    int i,it,contado;
    char ptmp[MAX_PAR];
    contado=0;
    while(text[i]!='\0'){
        saltarBlancs(text,&i);
        copiaParTemporal(text,&i,ptmp);
        printf("Num Paraules: %i",contadorPar(contado));
    }
}
```
![Contar Paraula](/imatges/20.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 21:
---
**Donat un text i una paraula (parbus) i una paraula (parsub).Dissenyeu un algorisme que substitueixi, en el text, totes les vegades que apareix la paraula (parbus) per la paraula (parsub).**

```c
#define MAX_PAR 100
#define MAX_TEXT 100
#define BB while (getchar()!='\n')

void entradaT(char text[]);
void entradaPa(char par[]);
void entradaParSub(char pSub[]);
void copiaText1Text2(char text[],int*i,char text2[]);
void copiaParTemp(char text[],int*i,char ptmp[]);
void buscarParaula(char ptmp[],char pBus[]);
void subParaula(char pSub[],int *isub,char text2[]);
void textSub(char text[],char pBus[],char pSub[]);
void imprimiText1(char text[]);
void imprimiText2(char text2[]);

int main()
{
    char text[MAX_TEXT+1],par[MAX_PAR+1],pSub[MAX_PAR+1],pBus[MAX_PAR+1],text2[MAX_TEXT+1];
    entradaT(text);
    entradaPa(par);
    entradaParSub(pSub);

    textSub(text,pBus,pSub);

    imprimiText1(text);
    imprimiText2(text2);
}
void entradaT(char text[]){
   printf("TEXT:");
    scanf("%100[^\n]",text);BB;
}

void entradaPa(char par[]){
    printf("PARAULA BUSCAR:");
    scanf("%100[^\n]",par);BB;
}

void entradaParSub(char pSub[]){
    printf("PARAULA SUBSTITUCIO:");
    scanf("%100[^\n]",pSub);BB;
}

void copiaText1Text2(char text[],int*i,char text2[]){
          int it2=0;
          while(text[*i]!=' ' && text[*i]!='\0'){
                text2[it2]=text[*i];
                it2++;
                (*i)++;
          }
}

void copiaParTemp(char text[],int*i,char ptmp[]){
          int itmp=0;
          while(text[*i]!=' ' && text[*i]!='\0'){
                ptmp[itmp]=text[*i];
                itmp++;
                (*i)++;
          }
         ptmp[itmp]='\0';
}

void buscarParaula(char ptmp[],char pBus[]){
    int ibus=0;
    while(ptmp[ibus]!='\0' || pBus[ibus]!='\0'){
                if(ptmp[ibus] != pBus[ibus]) break;
                ibus++;
            }
}

void subParaula(char pSub[],int *isub,char text2[]){
    int ibus=0;
    int it2=0;
    char ptmp[MAX_PAR],pBus[MAX_PAR];
    *isub=0;
     if(ptmp[ibus]=='\0' && pBus[ibus]=='\0'){
         while(pSub[*isub]!='\0'){
               text2[it2]=pSub[*isub];
               it2++;
               (*isub)++;
         }
     } else{
            while(ptmp[*isub]!='\0'){
               text2[it2]=ptmp[*isub];
               it2++;
               (*isub)++;
            }
        }
     text2[it2]='\0';
}

void textSub(char text[],char pBus[],char pSub[]){
    int i,isub;
    char ptmp[MAX_PAR];
    char text2[MAX_TEXT];

    while(text[i]!='\0'){
        saltarBlancs(text, &i);
        copiaText1Text2(text,&i,text2);
        copiaParTemp(text,&i,ptmp);
        buscarParaula(ptmp,pBus);
        subParaula(pSub,&isub,text2);
    }

}

void imprimiText1(char text[]){
    printf("TEXT 1 -> %s\n",text);
}

void imprimiText2(char text2[]){
    printf("TEXT 2 -> %s",text2);
}void entradaT(char text[]){
   printf("TEXT:");
    scanf("%100[^\n]",text);BB;
}

void entradaPa(char par[]){
    printf("PARAULA BUSCAR:");
    scanf("%100[^\n]",par);BB;
}

void entradaParSub(char pSub[]){
    printf("PARAULA SUBSTITUCIO:");
    scanf("%100[^\n]",pSub);BB;
}

void copiaText1Text2(char text[],int*i,char text2[]){
          int it2=0;
          while(text[*i]!=' ' && text[*i]!='\0'){
                text2[it2]=text[*i];
                it2++;
                (*i)++;
          }
}

void copiaParTemp(char text[],int*i,char ptmp[]){
          int itmp=0;
          while(text[*i]!=' ' && text[*i]!='\0'){
                ptmp[itmp]=text[*i];
                itmp++;
                (*i)++;
          }
         ptmp[itmp]='\0';
}

void buscarParaula(char ptmp[],char pBus[]){
    int ibus=0;
    while(ptmp[ibus]!='\0' || pBus[ibus]!='\0'){
                if(ptmp[ibus] != pBus[ibus]) break;
                ibus++;
            }
}

void subParaula(char pSub[],int *isub,char text2[]){
    int ibus=0;
    int it2=0;
    char ptmp[MAX_PAR],pBus[MAX_PAR];
    *isub=0;
     if(ptmp[ibus]=='\0' && pBus[ibus]=='\0'){
         while(pSub[*isub]!='\0'){
               text2[it2]=pSub[*isub];
               it2++;
               (*isub)++;
         }
     } else{
            while(ptmp[*isub]!='\0'){
               text2[it2]=ptmp[*isub];
               it2++;
               (*isub)++;
            }
        }
     text2[it2]='\0';
}

void textSub(char text[],char pBus[],char pSub[]){
    int i,isub;
    char ptmp[MAX_PAR];
    char text2[MAX_TEXT];

    while(text[i]!='\0'){
        saltarBlancs(text, &i);
        copiaText1Text2(text,&i,text2);
        copiaParTemp(text,&i,ptmp);
        buscarParaula(ptmp,pBus);
        subParaula(pSub,&isub,text2);
    }

}

void imprimiText1(char text[]){
    printf("TEXT 1 -> %s\n",text);
}

void imprimiText2(char text2[]){
    printf("TEXT 2 -> %s",text2);
}
```
![Substituir Paraula](/imatges/21.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>
